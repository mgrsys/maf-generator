import picocli.CommandLine

/**
 * @since 2018
 * @author Anton Vlasov - whalemare
 */
class Main {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val commandLine = CommandLine(InitCommand())
            commandLine.registerConverter(InitCommand.ArchType::class.java, { string ->
                return@registerConverter when (string) {
                    "java-mvp", "java_mvp" -> {
                        InitCommand.ArchType.JAVA_MVP
                    }
                    "java-mvvm", "java_mvvm" -> {
                        InitCommand.ArchType.JAVA_MVVM
                    }
                    "mvvm-dagger", "mvvm_dagger" -> {
                        InitCommand.ArchType.MVVM_DAGGER
                    }
                    else -> {
                        InitCommand.ArchType.valueOf(string.toUpperCase())
                    }
                }
            })
            commandLine.parseWithHandlers(
                CommandLine.RunLast(),
                System.out,
                CommandLine.Help.Ansi.AUTO,
                CommandLine.DefaultExceptionHandler(),
                *args
            )
        }
    }
}