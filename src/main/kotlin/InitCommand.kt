import org.apache.commons.io.FileUtils
import org.zeroturnaround.exec.ProcessExecutor
import picocli.CommandLine
import java.io.File

/**
 * @since 2018
 * @author Anton Vlasov - whalemare
 */
@CommandLine.Command(
    name = "clone",
    description = ["Clone maf and create based on them new project"]
)
class InitCommand : Runnable {

    @CommandLine.Option(
        names = ["-h", "--help"],
        description = ["Show help message for mira"]
    )
    val help: Boolean = false

    @CommandLine.Option(
        names = ["-s", "--source"],
        description = ["Source to template maf repository"]
    )
    val projectLink = "https://bitbucket.org/mgrsys/internal-android-blankproject"

    @CommandLine.Option(
        names = ["-n", "--name"], required = true,
        description = ["Name of your project"]
    )
    val packageProject = "" // com.magora.new_wonderful_project

    val projectName by lazy {
        packageProject.getLastSeparatedBy(".")
    }

    @CommandLine.Option(
        names = ["--maf"],
        description = ["Application package name of MAF", "By default = 'com.magora.maf'"]
    )
    val packageMaf = "com.mgrsys.androidblankproject"

    @CommandLine.Option(
        names = ["--arch"], description = ["Architecture type of generated project",
            "Kotlin MVVM-Toothpick by default",
            "@|bold,underline Values|@: @|fg(yellow) mvvm, mvvm_dagger, java_mvvm, java_mvp|@",
            ""]
    )
    val archType: ArchType = ArchType.MVVM

    val mafPrefix by lazy { packageMaf.split(".")[0] }
    val mafGroup by lazy { packageMaf.split(".")[1] }
    val mafName by lazy { packageMaf.split(".")[2] }
    val mafSlash by lazy { packageMaf.replace(".", "/") }

    val sampleFolder = "app"
    val frameworkFoler = "maf"

    override fun run() {
        checkProjectName(packageProject)
        checkProjectName(packageMaf)

        archType.clone(projectLink)

        val files = File(".")
        val clonedFolder = files.listFiles().find {
            it.name == projectLink.getLastSeparatedBy("/")
        }
        val successfullyCloned = clonedFolder != null

        if (successfullyCloned) {
            println("1. Create $projectName folder for project")
            val appFolder = createFolder(projectName)

            println("2. Copy all from maf-framework [${clonedFolder!!.name}] to app-folder [${appFolder.name}]")
            clonedFolder.copyTo(appFolder)

            println("3. Rename packages to new paths")
            appFolder.renamePackagesRecursively(packageMaf, packageProject)

            println("4. Delete empty folders on $appFolder")
            appFolder.deleteEmptyFolders()

            println("5. Remove old git from $appFolder")
            removeGit(appFolder)

            println("6. Rename files package name from $packageMaf to $packageProject")
            renameFilesContent(appFolder, packageMaf, packageProject)

            println("7. Remove cloned temp folder $clonedFolder")
            FileUtils.deleteDirectory(clonedFolder)

            println("project $packageProject successfully generated")
        } else {
            println("$frameworkFoler not found, abort initialization")
            return
        }
    }

    fun createFolder(name: String): File {
        val root = File(name)
        if (!root.exists()) {
            println("Folder $name already exist. Just return existed folder")
            root.mkdir()
        }
        return root
    }

    private fun checkProjectName(projectName: String) {
        if (projectName.split(".").size != 3) {
            throw IllegalArgumentException(
                "The name of app should consist of 3 parts separated by dots.\n" +
                        "You should call it as like \"maf com.magora.appname\""
            )
        }
        if (projectName.contains("/")) {
            throw IllegalArgumentException("The name of app cannot contain a slash sign '/'")
        }
    }

    private fun renameFilesContent(where: File, from: String, to: String) {
        where.listFiles().forEach { file ->
            if (file.isDirectory) {
                renameFilesContent(file, from, to)
            } else {
                var content = file.readText()
                content = content.replace(from, to)
                file.writeText(content)
            }
        }
    }

    private fun renamePackages(rootNewApp: File) {
        val projectPath = packageProject.replace(".", "/")

        val newAppSrc = File(rootNewApp.path, "app/src")
        newAppSrc.listFiles().forEach { dir ->
            FileUtils.deleteDirectory(dir)
        }

        val mafSrc = File(getMafFiles(), "app/src")
        mafSrc.listFiles().forEach { dir ->
            val sources = File(dir, "java/$mafSlash")
//            "app/src/java/$projectPath/${dir.name}"
            FileUtils.copyDirectory(
                sources,
                File(rootNewApp, "app/src/${dir.name}/java/${projectPath}")
            )
        }

        val mainSrc = File(getMafFiles(), "app/src/main")
        mainSrc.listFiles().forEach { from ->
            if (from.name != "java" && from.name != "kotlin") {
                val to = File(rootNewApp.path, "app/src/main/${from.name}")
                from.copyTo(to)
            }
        }
    }

    private fun removeGit(from: File) {
        val dirGit = from.listFiles().find {
            it.name == ".git"
        }
        dirGit?.let {
            FileUtils.deleteDirectory(dirGit)
        }
    }

    private fun createStructure(projectName: String, from: File): File {
        val root = File(projectName)
        if (!root.exists()) {
            root.mkdir()
        }
        from.copyTo(root)
        return root
    }

    private fun getMafFiles(): File {
        return File(frameworkFoler)
    }

    fun File.copyTo(folder: File) {
        when {
            isDirectory -> FileUtils.copyDirectory(this, folder)
            isFile -> FileUtils.copyFile(this, folder)
            else -> throw UnsupportedOperationException("${this.name} is not folder, and not file. It`s exist ?= ${this.exists()}")
        }
    }

    companion object {
        fun exec(command: () -> String): String {
            println(command())
            val output = executor(command)
                .destroyOnExit()
                .readOutput(true)
                .execute()
                .outputUTF8()
            return output
        }

        fun executor(command: () -> String): ProcessExecutor {
            return ProcessExecutor()
                .command(command.invoke().split(" "))
                .redirectOutput(object : org.zeroturnaround.exec.stream.LogOutputStream() {
                    override fun processLine(line: String?) {
                        line?.let { println(it) }
                    }
                })
        }
    }

    enum class ArchType(val initFunction: (String) -> Unit) {
        MVVM({
            executor { "git checkout feature/kotlin-mvvm-toothpick" }
                .directory(File(it))
                .execute()
        }),
        MVVM_DAGGER({
            executor { "git checkout feature/kotlin-mvvm" }
                .directory(File(it))
                .execute()
        }),
        JAVA_MVP({
            executor { "git checkout feature/master" }
                .directory(File(it))
                .execute()
        }),
        JAVA_MVVM({
            //            exec { "git checkout feature/MVVM-BRANCH-NAME" }
            kotlin.io.println("mvvm not defined. Used default mvvm with toothpick")
            InitCommand.ArchType.MVVM.initFunction.invoke(it)
        });

        fun clone(projectLink: String) {
            exec { "git clone $projectLink" }
            val folderName = projectLink.getLastSeparatedBy("/")
            initFunction.invoke(folderName)
        }
    }
}

private fun File.deleteEmptyFolders() {
    val emptyFolders = mutableListOf<File>()
    seekEmpty(this, emptyFolders)
    emptyFolders.forEach {
        it.delete()
    }
    emptyFolders.clear()

    seekEmpty(this, emptyFolders)
    if (emptyFolders.isNotEmpty()) {
        this.deleteEmptyFolders()
    }
//    listFiles().filter { it.isDirectory }.forEach { folder ->
//        if (folder.listFiles().isEmpty()) {
//            val deleted = folder.delete()
//            if (!deleted) {
//                println("Cant delete $folder")
//            }
//        } else {
//            folder.deleteEmptyFolders()
//        }
//    }
}

/**
 *  Rename all packages with "from" structure to "to" structure
 *  from: com.mgrsys.androidblankproject
 *  to: com.magora.app
 */
private fun File.renamePackagesRecursively(from: String, to: String) {
    val fromSlashed = from.replace(".", "/")
    val toSlashed = to.replace(".", "/")

    val froms = from.split(".")
    val tos = to.split(".")

    val filesPaths = mutableListOf<File>()
    lookInside(this, filesPaths)

    if (froms.size == tos.size) {
        filesPaths.forEach { file ->
            if (file.path.contains(fromSlashed)) {
                val oldPath = file.path
                val newPath = oldPath.replace(fromSlashed, toSlashed)
                File(oldPath).moveTo(File(newPath))
            }
        }
    } else {
        throw IllegalArgumentException("Package depths must be equals")
    }
}

/** This method goes through all folders that are directly contained in a
 * directory, and appends them to a list if they are empty. Used
 * recursively for directories containing multiple folders
 *
 * @param dir is the directory to check for content
 * @param emptyFolders is he List we add empty folders to.
 */
private fun seekEmpty(dir: File, emptyFolders: MutableList<File>) {

    // Populate an array with files/folders in the directory
    val folderContents = dir.listFiles()
    if (folderContents!!.size == 0) {
        // we are empty, add us.
        emptyFolders.add(dir)
    }

    // Iterate through every file/folder
    for (content in folderContents) {
        // Disregard files, acquire folders
        if (content.isDirectory) {
            // check if this folder is empty
            seekEmpty(content, emptyFolders)
        }
    }
}

/**
 * Get paths to all files
 */
fun lookInside(folder: File, list: MutableList<File>) {
    folder.listFiles().forEach { file ->
        if (file.isFile) {
            list.add(file)
        } else {
            lookInside(file, list)
        }
    }
}

fun File.moveTo(folder: File) {
    when {
        isDirectory -> FileUtils.moveDirectory(this, folder)
        isFile -> FileUtils.moveFile(this, folder)
        else -> throw UnsupportedOperationException("${this.name} is not folder, and not file. It`s exist ?= ${this.exists()}")
    }
}

/**
 * from: com.magora.app
 * to: app
 */
fun String.getLastSeparatedBy(symbol: String): String {
    val position = lastIndexOf(symbol)
    return substring(position + 1)
}