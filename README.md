# MAF Generator
Эта штука генерирует структуру проекта по архитектуре MAF

# Как использовать
### Новый способ
0. Генерируем .jar с помощью гредл комманды `./gradlew shadowJar` или см. п.1
1. В папке `release` берем последний *.jar
2. Перемещаем ее в папку, где необходимо инициализовать проект
(к примеру в /Users/whalemare/MagoraProjects)
3. Открываем терминал (консоль) в этой папке
4. Запускаем jar командой ниже, передавая аргументом название приложения с указанием пакета
```
java -jar maf.jar --name com.magora.appname
```
5. Ждем пока проект сгенерируется и открываем его через Android Studio
6. Для удобства можете прописать быстрый запуск через алиас
пример для mac os: alias maf='java -jar maf.jar'

Список всех доступных команд смотри с помощью команды `-help`

### Cтарый способ
1. Перейдите в класс Maf
2. Запустите метод main
3. Поймите, что необходимо указать название приложения как аргумент
(Вверху, рядом с треугольником надпись Maf -> Edit Configurations -> Program Arguments)
напишите название приложение в виде "com.magora.trololo" без кавычек.
4. Снова запустите программу
5. Сгенерируется проект с вашим названием и встреонной архитектурой MAF

# В планах
~~1. Добавить пул проекта из репозитория (сейчас это просто папка внутри maf-generator)~~
~~2. Собрать standalone-jar для возможности удобного использования без IDE~~
~~3. Написать более подробную документацию по запуску~~
~~4. Нужно потестить на бою~~
~~5. Написать документацию к скрипту~~
~~6. Выбор архитектуры и языка генерируемого проекта~~
~~7. Использовать реальный maf https://bitbucket.org/mgrsys/internal-android-blankproject вместо копии~~

